﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace EncodingTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () => await GetStuff());

            Console.ReadLine();
        }

        static async Task GetStuff()
        {
            var client = new HttpClient();

            var res = await client.GetAsync("https://winmxunlimited-api.herokuapp.com/testendpoint");

            var items = await res.Content.ReadAsAsync<List<UserItem>>();

            Console.WriteLine(items.Count);
        }
    }


    class UserItem
    {
        public string roomname { get; set; }
        public string username { get; set; }
        public uint filecount { get; set; }
        public ushort connection { get; set; }
        public uint primaryip { get; set; }
        public ushort primaryport { get; set; }
        public byte rank { get; set; }
    }
}
